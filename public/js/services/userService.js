(function() {

  'use strict';

  angular

   .module('app')

  .service('userService', function($http) {

    var root = this;
    var users = this.users;

    this.list = function() {

      $http.get('/api/users')

      .success(function(data) {
          users = data;
          // return data;
          console.log(data);
        })
        .error(function(data) {
          return data;
        });
    }

  });

})();