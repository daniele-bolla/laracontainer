(function() {

  'use strict';

  angular

   .module('app')

  .service('authService', function($http, $window, $location) {

    var root = this;

    this.login = function(data) {
      $http.post('/api/login', data).success(function(success) {

        $window.localStorage.setItem("session", success.token);
        $location.path('/posts');

      }).error(function(error) {

        console.log(error);
      });

    }

    this.signup = function(data) {

      $http.post('/api/users', data).success(function(success) {

        var registerlog = {
          password: data.password,
          email: data.email
        };

        root.login(registerlog);

      }).error(function(error) {
        console.log(error);
      });

    }

    this.logout = function() {

      $window.localStorage.removeItem("session");

      $location.path('/home');

    }

    this.logged = function() {
      var user = angular.fromJson($window.atob($window.localStorage.session.split('.')[1]));
      console.log(user);
      return user;
    }



  });

})();