(function() {

    'use strict';

    angular
        .module('app')
        .directive('navbar', function() {
          return {
            restrict: 'E',

            require: '^pulse',
            controller : "templateController", 
            scope: {
              menus :'=',
              brand : '@',
              styles :"@",
              buttonAction :"&"
            },

            templateUrl: '/js/directives/navbar/navbar.tpl.html'
          };
        });
  
})();