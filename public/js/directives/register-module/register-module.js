(function() {

  'use strict';

  angular
    .module('app')
    .directive('registerModule', ['authService', function(authService) {
      return {
        restrict: 'E',
        replace: true,
        //transclude : true,
        scope: {
          head: '@',
          style: '@',
          action: '&'
        },

        templateUrl: '/js/directives/register-module/register-module.tpl.html',

        /** Logic directive **/
        controller: function($scope, $element,   $http ) {
          var fields = $element.find('input');
          fields.bind('blur', function(){
          var n =   this.attributes['name'].value;
          var v = this.value;
          var param = {name : n, val : v };
           alert( this.value );
            $http.post('api/check', param).then(function(data) {
              console.log(data);
            });
          });
          $scope.action = authService.signup;

          /** Popula directive **/
          $scope.form = {

            buttons: [{
              'txt': 'Sign Up',
              'style': ' btn-info btn-block'
            }]

          }

        },
        controllerAs: 'reg',
        bindToController: true,
        /*link: function(scope, elem, attrs, reg) {
                elem.on('blur', function(evt) {
                  alert(234);
                  scope.$apply(function() {
                    $http({
                      method: 'POST',
                      url: 'api/check',
                      data: {
                        username: elem.val(),
                        dbField: attrs.ngUnique
                      }
                    }).success(function(data, status, headers, config) {
                      ctrl.$setValidity('unique', data.status);
                    });
                  });
                });
        }
        */
      };
    }]);

})();