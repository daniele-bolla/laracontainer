(function() {

  'use strict';

  angular
    .module('app')
    .directive('logoutPulse', ['authService', function(authService) {
      return {
        restrict: 'E',
        // replace: true,

       scope: {

        },
        templateUrl: '/js/directives/logout-pulse/logout-pulse.tpl.html',
        link: function(s) {
            s.test = authService.logged();
        }
      };
    }]);

})();