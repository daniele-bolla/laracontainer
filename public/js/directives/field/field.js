(function() {

    'use strict';

    angular
        .module('app')
        .directive('field', function() {
          return {
            restrict: 'E',
            replace: true,
          require: "?ngModel",
            scope: {
              ngModel :'=',
              label :'@',
              type :'@',
              name :'=',
              id :'@',
            },
            link: function( s, e, a, ngModel) {
              s.test = ngModel;
            },
            templateUrl: '/js/directives/field/field.tpl.html'
          };

        });
})();