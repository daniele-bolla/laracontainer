  (function() {

  'use strict';

  angular
    .module('app')
    .directive('loginModule', ['authService', function(authService) {
      return {
        restrict: 'E',
        replace: true,

        scope: {
          head: '@',
          style: '@'
          //action: '&'
        },

        templateUrl: '/js/directives/login-module/login-module.tpl.html',
       
        /** Logic directive **/
        controller: function($scope) {

          $scope.action = authService.login;
          //$scope.users = authService.list();
          /** Popula directive **/
         // $scope.log.email = 'asd'; 
          $scope.form = {
            
            buttons: [
              {
              'txt' : 'Login',
              'style': ' btn-success'
              }
            ]
            
          }

        },
        controllerAs: 'log',
        bindToController: true
        
      };
    }]);

})();