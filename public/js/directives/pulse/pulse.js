(function() {

    'use strict';

    angular
        .module('app')
        .directive('pulse', function() {
          return {
            restrict: 'E',
           // replace: true,

            scope: {
              txt :'@',
              c :'@',
              action :'&'
            },
            templateUrl: '/js/directives/pulse/pulse.tpl.html',
           link: function(s) {
            // s.test = authService.logged;
            } 
          };
        });
  
})();