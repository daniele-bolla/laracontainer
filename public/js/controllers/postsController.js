(function() {

    'use strict';

    angular
        .module('app')
        .controller('postsController', function($scope, $http){
        
        $scope.lists =[ ];
      
      
      /** List Function **/
        $scope.list = function(){
          
          $http.get('/api/posts')

            .success(function(data){
               var i= -1;
               angular.forEach(data, function(post) {
               
                 /*Qui richiamo il metodo show di laravel per ogni post così da poter lavorare con eloquent => $post->tag->list(); */

                 $http.get('/api/posts/' + post.id).success(function(item){
                  i++;
                       $scope.lists[i]=item;	
                      // console.log( $scope.lists);	 

                  });
              });
          })
            .error(function(data){
          });

        }
      
        // run function onload
        $scope.list();
      /**End List Function **/      
      
     /** Create Function **/
        $scope.create = function(){
          
          $http.post('/api/posts', $scope.post)

            .success(function(data){
             alert('Post Created');
           // console.log(data);
             $scope.list();
          })
            .error(function(data){
            // console.log(data);
          });

        }
      /**End Create Function **/     
        
      /** Delete Function **/
        $scope.update = function(id){
          
          $http.put('/api/posts/' + id)

            .success(function(data){
            // $scope.list();
          })
            .error(function(data){
          });

        }
      /**End Delete Function **/      
        
       /** Delete Function **/
        $scope.delete = function(id){
          
          $http.delete('/api/posts/' + id)

            .success(function(data){
             $scope.list();
          })
            .error(function(data){
          });

        }
      /**End Delete Function **/
  
    });
  
})();