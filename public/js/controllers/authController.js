(function() {

    'use strict';

    angular
        .module('app')
        .controller('authController', function($rootScope, $scope, $http, $location, $window){
      
        $scope.lists ={};        
        $scope.errors ={};           
       // $scope.data ={};
      
        $rootScope.logged = false;
      
      
      /** List Function **/
        $scope.list = function(){
          
          $http.get('/api/users')

            .success(function(data){
               var i= 0;
               angular.forEach(data, function(user) {
               
                 /*Qui richiamo il metodo show di laravel per ogni user così da poter lavorare con eloquent => $user->posts->list(); */

                 $http.get('/api/users/' + user.id).success(function(item){
                  i++;
                       $scope.lists[i]=item;	
                      // console.log( $scope.lists);	 

                  });
              });
          })
            .error(function(data){
            $scope.error = data;
          });

        }
      
        // run function onload
        $scope.list();
      /**End List Function **/  
      
     /** Create Function **/
        $scope.create = function(){
          
          $http.post('/api/users', $scope.user)

            .success(function(data){
             console.log(data);
          })
            .error(function(data){
          });

        }
      /**End Create Function **/     
        $scope.data = {};
      /** Login Logged Logout Function **/
        $scope.login = function(){
           $scope.$watch("data", function(newvalue,oldvalue){
                console.log(newvalue);
              })
          $http.post('/api/login', $scope.auth)
          

            .success(function(data){
            $rootScope.logged = true;
            
            $window.localStorage.setItem("session", data.token);
            $location.path('/posts');
          })
            .error(function(data){
          //  console.log(data);
          });
          
        }          
     
              
        $rootScope.logout = function(){
            $window.localStorage.removeItem("session");
            $rootScope.logged = false;
            $location.path('/home');
        }
      /**End Login Logged Logout  Function **/     
        
      /** Delete Function **/
        $scope.delete = function(id){
          
          $http.delete('/api/posts/' + id)

            .success(function(data){
             $scope.list();
          })
            .error(function(data){
          });

        }
      /**End Delete Function **/      
        
      /** Delete Function **/
        $scope.update = function(id){
          
          $http.put('/api/posts/' + id)

            .success(function(data){
            // $scope.list();
          })
            .error(function(data){
          });

        }

    });
  
})();