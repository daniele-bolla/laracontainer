(function() {

    'use strict';

    angular
        .module('app')
        .controller('tagsController', function($scope, $http){
        
        $scope.lists ={};
      
      
      /** List Function **/
        $scope.list = function(){
          
          $http.get('/api/tags')

            .success(function(data){
               var i= -1;
               angular.forEach(data, function(post) {
               
                 /*Qui richiamo il metodo show di laravel per ogni post così da poter lavorare con eloquent => $post->tag->list(); */

                 $http.get('/api/tags/' + post.id).success(function(item){
                  i++;
                       $scope.lists[i]=item;	
                     //  console.log( item.posts);	 

                  });
              });
          })
            .error(function(data){
          });

        }
      
        // run function onload
        $scope.list();
      /**End List Function **/      
      
     /** Create Function **/
        $scope.create = function(){
          
          $http.post('/api/tags', $scope.tag)

            .success(function(data){
             alert('Post Created');
            //console.log(data);
             $scope.list();
          })
            .error(function(data){
            // console.log(data);
          });

        }
      /**End Create Function **/     
        
      /** Delete Function **/
        $scope.delete = function(id){
          
          $http.delete('/api/tags/' + id)

            .success(function(data){
             $scope.list();
          })
            .error(function(data){
          });

        }
      /**End Delete Function **/
  
    });
  
})();