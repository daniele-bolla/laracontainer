(function() {

    'use strict';

    angular
        .module('app', ['ngRoute','ngAnimate'])
    
        .config(['$routeProvider' , function($routeProvider){

        $routeProvider.when('/home',{
          templateUrl : 'view/home.html',
          //controller : 'authController'
        }).when('/users',{
          templateUrl : 'view/users.html',
          controller : 'authController'
        }).when('/posts',{
          templateUrl : 'view/post.html',
          controller : 'postsController'
        }).when('/tags',{
          templateUrl : 'view/tag.html',
          controller : 'tagsController'
        }).otherwise({
          redirectTo : '/home'
        });
      }])
     .config(['$httpProvider', function($httpProvider){
       
        $httpProvider.interceptors.push(['$rootScope', '$q',  '$location','$window',
           function ($rootScope, $q,  $location, $window) {
               return {
                   'request': function (config) {
                        config.headers = config.headers || {};
                        if ($window.localStorage.session) {
                            config.headers.Authorization = 'Bearer ' + $window.localStorage.session;
                        }
                        return config;
                    },
                    'response': function (res) {
                        if (res.data.status === 401 || res.data.status === 500) {
                            $location.path('/api/login');
                            $location.replace();
                        }
                        return res || $q.when(res);
                    }
               };
           }
        ]);
       
     }]);
  
})();

