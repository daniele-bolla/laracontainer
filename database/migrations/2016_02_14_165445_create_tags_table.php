<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
    {
		Schema::create('tags', function(Blueprint $table)
		{
			$table->increments('id')->nullable();
			$table->text('name');
			$table->timestamps();
		});		
		Schema::create('posts_tags', function(Blueprint $table)
		{
			//$table->dropForeign('posts_tags_posts_id');
 			$table->integer('posts_id')->unsigned()->index();
			$table->foreign('posts_id')->references('id')->on('posts')->onDelete('cascade');
			//$table->dropForeign('posts_tags_tags_id');
 			$table->integer('tags_id')->unsigned()->index();
			$table->foreign('tags_id')->references('id')->on('tags')->onDelete('cascade');
			$table->timestamps();
		});

    }
		
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('tags');
		    Schema::drop('post_tag');
    }
}
