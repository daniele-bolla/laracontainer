<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\User;
use Hash;
use Input;

class authController extends Controller
{
  
    //
  
  public function __construct()
   {
      // $this->middleware('jwt.auth', ['except' => ['login','store','index']]);
   }
  
  
  public function index(){
				if($users = User::all()){
					return $users;
				} else {
					return response()->json(['error' => 'access_denied'], 401);
				}
  }
  
  public function login(Request $request)
  {
      $credentials = $request->only('email', 'password');

      try {
          // verify the credentials and create a token for the user
          if (! $token = JWTAuth::attempt($credentials)) {
              return response()->json(['error' => 'invalid_credentials'], 401);
          }
      } catch (JWTException $e) {
          // something went wrong
          return response()->json(['error' => 'could_not_create_token'], 500);
      }

      // if no errors are encountered we can return a JWT
      return response()->json(compact('token'));
  }
  
  public function store(Request $request){
    $user = $request->all();
    $user['password'] =  Hash::make($user['password']);
    User::create($user);
    return $user;
  }
  
  public function show($id, Request $request)
  {

    $user = User::findOrFail($id);

    $user['posts'] = $user->posts()->lists('tit');

    return $user;
  }	  
	
	public function check(Request $request)
  {
	$email =	$request->get('email');
		
	/*	if (User::where('email', '=', $request->get('email'))->exists()) {
			 // user found
			return 'user found';
		} else {
			return "ok";
		}
*/
		return $email;
    
  }		
  
}
