<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tags;
use Carbon\Carbon; 

class tagsController extends Controller
{
    //
	public function __construct()
	{
			$this->middleware('jwt.auth');
	}
	
  public function index()
	{
    	// list all
	    $tags = Tags::all();
	    return $tags;
	}    
	
	public function store(Request $request)
	{
  		// save new item
	    $data = $request->all();
		  Tags::create($data);
	    return "Tag Created";
	}
	
	public function show($id, Request $request)
		{
			//delte item
			$tag = Tags::findOrFail($id);
	
			$tag['posts'] = $tag->posts()->lists('tit');

			return $tag;
		}	
	
	public function update($id, Request $request)
	{
		//update item	
      $post = Posts::findOrFail($id);
      $data = $request->all();
      $data['updated_at'] = Carbon::now();
      $post->update($data);
	
	    return "Tag Updated";
	}	
	
	public function destroy($id)
	{
		//delte item
	    $post = Tags::findOrFail($id);
		$post->delete();
	
	    return "Tag Deleted";
	}
}
