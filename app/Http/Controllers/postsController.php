<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Posts;

class postsController extends Controller
{
    //
		public function __construct()
		 {

				// $this->middleware('jwt.auth');
		 }
	
		public function index()
		{
				// list all
				$data = Posts::all();
			//$data->tags()->list('name');
				return $data;
		} 
	
		public function store(Request $request)
		{
				// save new item
				//Tags::list('name');
		//JWTAuth::parseToken();

		// and you can continue to chain methods
			$user = JWTAuth::parseToken()->authenticate();
			$data = $request->all();
			$data['user_id'] = $user->id;

			$tags = Input::get('tags');
			//
			$post = Posts::create($data);

			$post->tags()->attach($tags);

				return 'Post Created';
		}
	
		public function show($id, Request $request)
		{
			//delte item
			$post = Posts::findOrFail($id);
			$post['writers'] = $post->user()->lists('name');
			$post['tags'] = $post->tags()->lists('name');

			return $post;
		}	
	
		public function update($id, Request $request)
		{
			//update item	
			$post = Posts::findOrFail($id);
			$data = $request->all();
			$tags = Input::get('tags');
		//	$data['updated_at'] = Carbon::now();
			$post->update($data);
			$post->tags()->attach($tags);

				return "Post Updated";
		}
	
		public function destroy($id, Request $request)
		{
			//delte item
			$post = Posts::findOrFail($id);
			$post->delete();

			return "Post Deleted";
		}	
	
	
}
