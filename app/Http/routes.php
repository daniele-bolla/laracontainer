<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| RestFull Api Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api'], function(){   
    Route::resource('/tags', 'tagsController');
    Route::resource('/posts','postsController');    
    Route::resource('/rates','ratesController');
    Route::resource('/users','authController');
    Route::resource('/comments','commentsController');
    Route::post('/login', 'authController@login');
    Route::post('/check', 'authController@check');
});
