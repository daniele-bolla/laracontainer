<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    //
     protected $fillable = [
        'name'
    ];
	
	function posts (){
		return $this->belongsToMany('App\Posts');
	}
}
